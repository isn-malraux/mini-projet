#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Traitement d'image

@author: Grégory DAVID
"""

import PIL.Image

cheminImage = None  # contiendra le chemin d'accès au fichier image
fichierImage = None  # contiendra l'image source à traiter


def ouvrirImage(fichierSource=None):
    """Effectue l'ouverture d'un fichier image fourni en paramètre
    et stocke le contenu de l'image dans la variable globale fichierImage.

    Si la variable globale fichierImage existe et contient quelque chose,
    la fonction ne fait rien.
    """
    global cheminImage
    global fichierImage
    if fichierImage is not None:
        return fichierImage
    else:
        while fichierSource in [None, '']:
            fichierSource = str(input("Donner le chemin d'accès (absolu ou relatif) au fichier image :\n> "))
        cheminImage = fichierSource
        try:
            fichierImage = PIL.Image.open(cheminImage)
            return fichierImage
        except FileNotFoundError or PermissionError or IOError:
            print("Le fichier", cheminImage, "n'est pas accessible.")
            quitterProgramme(128)


def afficherInfoImage(imageSource):
    """Affiche les meta informations de l'image passée en paramètre.
    """
    global cheminImage
    nettoyerEcran()
    print("Propriétés de l'image", cheminImage, ":")
    print("\tmode colorimétrique :", imageSource.mode)
    print("\tformat de l'image :", imageSource.format)
    print("\tdimensions : ", imageSource.size[0], "x", imageSource.size[1], sep='')
    if imageSource.mode in ['JPEG', 'PNG', 'TIFF']:
        print("\tresolution : ", imageSource.info['dpi'][0], "x", imageSource.info['dpi'][1], sep='')


def negatifCouleur(imageSource):
    """Transforme l'image passée en paramètre afin de la rendre négative.
    """
    if imageSource is not None:
        imageDestination = imageSource.copy()
        pixels = imageDestination.load()
        composantes = imageDestination.getbands()
        for y in range(imageDestination.size[1]):
            for x in range(imageDestination.size[0]):
                """Faire attention aux différents modes sur une composante.
                Voir https://github.com/python-pillow/Pillow/blob/2.9.x/docs/handbook/concepts.rst

                1 (1-bit pixels, black and white, stored with one pixel per byte)
                L (8-bit pixels, black and white)
                P (8-bit pixels, mapped to any other mode using a color palette)
                RGB (3x8-bit pixels, true color)
                RGBA (4x8-bit pixels, true color with transparency mask)
                CMYK (4x8-bit pixels, color separation)
                YCbCr (3x8-bit pixels, color video format)
                LAB (3x8-bit pixels, the L*a*b color space)
                HSV (3x8-bit pixels, Hue, Saturation, Value color space)
                I (32-bit signed integer pixels)
                F (32-bit floating point pixels)
                """
                if composantes[0] in ['L', 'P']:
                    pixels[x, y] = 255 - pixels[x, y]
                elif composantes[0] == 'I':
                    pixels[x, y] = (2**32 - 1) - pixels[x, y]
                elif composantes[0] in ['1', 'F']:
                    pixels[x, y] = 1 - pixels[x, y]
                else:
                    pixel = list()
                    for composante in range(len(composantes)):
                        pixel.append(255 - pixels[x, y][composante])
                    pixels[x, y] = tuple(pixel)
        return imageDestination
    else:
        return imageSource


def rotationCouleur(imageSource, mode='RGB'):
    """Effectue une rotation colorimétrique de l'image passée en paramètre.

    Les modes de rotation sont les suivants :
        * RGB (rien à faire)
        * BGR (échange entre R et B)
        * BRG (rotation d'un rang vers la droite : R->G->B->R)
        * GBR (rotation de deux rangs vers la droite : R->B->G->R)
    """
    if imageSource is not None and imageSource.mode == "RGB":
        imageDestination = imageSource.copy()
        largeurImage = imageSource.size[0]
        hauteurImage = imageSource.size[1]
        pixels = imageDestination.load()
        if mode == 'RGB':
            pass
        elif mode == 'BGR':
            for y in range(hauteurImage):
                for x in range(largeurImage):
                    R, G, B = pixels[x, y]
                    pixels[x, y] = (B, G, R)
        elif mode == 'BRG':
            for y in range(hauteurImage):
                for x in range(largeurImage):
                    R, G, B = pixels[x, y]
                    pixels[x, y] = (B, R, G)
        elif mode == 'GBR':
            for y in range(hauteurImage):
                for x in range(largeurImage):
                    R, G, B = pixels[x, y]
                    pixels[x, y] = (G, B, R)
        else:
            return imageSource
        return imageDestination
    else:
        return imageSource


def rotationImage(imageSource, direction='gauche'):
    """Effectue la rotation 'physique' de l'image.

    Les rotations possibles sont :
        * gauche : Rotation de 90° anti-horaire
        * droite : Rotation de 90° horaire
        * retourner : Rotation de 180°
    """
    if imageSource is not None:
        if direction == "droite":
            return imageSource.transpose(PIL.Image.ROTATE_270)
        elif direction == "gauche":
            return imageSource.transpose(PIL.Image.ROTATE_90)
        elif direction == "retourner":
            return imageSource.transpose(PIL.Image.ROTATE_180)
        else:
            return imageSource
    else:
        return imageSource


def symetrieImage(imageSource, axe='V'):
    """Effectue une symétrie axiale de l'image passée en paramètre,
    selon l'axe passé en paramètre.

    Les axes possibles sont :
        * V, pour une symétrie axiale verticale (symétrie d'axe horizontal)
        * H, pour une symétrie axiale horizontale (symétrie d'axe vertical)
    """
    if imageSource is not None:
        if axe == 'V':
            return imageSource.transpose(PIL.Image.FLIP_TOP_BOTTOM)
        elif axe == 'H':
            return imageSource.transpose(PIL.Image.FLIP_LEFT_RIGHT)
        else:
            return imageSource
    else:
        return imageSource


def menu():
    """Affiche un menu et appelle les fonctions associées.
    """
    global fichierImage
    afficherMenuPrincipal = True
    options = [['O', "Ouvrir une image (et l'afficher)"],
               ['A', "Afficher l'image chargée (la charger au besoin)"],
               ['I', "Afficher les informations sur l'image"],
               ['N', "Mettre l'image en négatif"],
               ['C', "Effectuer la rotation des couleurs de l'image"],
               ['R', "Effectuer la rotation physique de l'image"],
               ['S', "Effectuer une transformation symétrique de l'image"],
               ['Q', "Quitter le programme"]]
    nomOptions = [uneOption[0] for uneOption in options]
    while afficherMenuPrincipal:
        afficherMenuPrincipal = False
        nettoyerEcran()
        print("Que voulez-vous faire ?")
        for option in options:
            print("\t", option[0], option[1], sep=' - ')
        selection = input("\n\t\tChoix : ").upper()
        if selection in nomOptions:
            if selection == 'O':
                fichierImage = None
                ouvrirImage().show()
                afficherMenuPrincipal = True
            elif selection == 'A':
                ouvrirImage().show()
                afficherMenuPrincipal = True
            elif selection == 'I':
                afficherInfoImage(ouvrirImage())
                input("Appuyer sur une 'entrée' pour revenir au menu ...")
                afficherMenuPrincipal = True
            elif selection == 'N':
                negatifCouleur(ouvrirImage()).show()
                afficherMenuPrincipal = True
            elif selection == 'C':
                rotationModes = ['RGB', 'BGR', 'BRG', 'GBR']
                afficherMenuRotationColorimetrique = True
                while afficherMenuRotationColorimetrique:
                    nettoyerEcran()
                    print("Quel mode de rotation colorimétrique voulez-vous ?")
                    for mode in rotationModes:
                        print("\t - ", mode, " - rotation de RGB->", mode, sep='')
                    print("\t - Q - Revenir au menu principal")
                    selection = input("\n\t\tChoix : ").upper()
                    if selection in rotationModes:
                        rotationCouleur(ouvrirImage(), selection).show()
                    elif selection == 'Q':
                        afficherMenuRotationColorimetrique = False
                        afficherMenuPrincipal = True
            elif selection == 'R':
                rotationDirections = ['gauche', 'droite', 'retourner']
                afficherMenuRotationImage = True
                while afficherMenuRotationImage:
                    nettoyerEcran()
                    print("Quelle direction de rotation physique voulez-vous ?")
                    for direction in rotationDirections:
                        print("\t", direction, sep=' - ')
                    print("\n\t - Q - Revenir au menu principal")
                    selection = input("\n\t\tChoix : ").lower()
                    if selection in rotationDirections:
                        rotationImage(ouvrirImage(), selection).show()
                    elif selection.upper() == 'Q':
                        afficherMenuRotationImage = False
                        afficherMenuPrincipal = True
            elif selection == 'S':
                symetrieAxes = [['V', 'Symétrie verticale (ou symétrie d\'axe horizontal),\n\t\t\t=> renverse l\'image de haut en bas'],
                                 ['H', 'Symétrie horizontale (ou symétrie d\'axe vertical),\n\t\t\t=> renverse l\'image de gauche à droite']]
                nomAxes = [unAxe[0] for unAxe in symetrieAxes]
                afficherMenuSymetrieImage = True
                while afficherMenuSymetrieImage:
                    nettoyerEcran()
                    print("Selon quel axe voulez-vous effectuer la symétrie ?")
                    for axe in symetrieAxes:
                        print("\t", axe[0], axe[1], sep=' - ')
                    print("\n\t - Q - Revenir au menu principal")
                    selection = input("\n\t\tChoix : ").upper()
                    if selection in nomAxes:
                        symetrieImage(ouvrirImage(), selection).show()
                    elif selection == 'Q':
                        afficherMenuSymetrieImage = False
                        afficherMenuPrincipal = True
            elif selection == 'Q':
                afficherMenuPrincipal = False
                quitterProgramme()
        else:
            afficherMenuPrincipal = True


def nettoyerEcran():
    """Effectue un nettoyage de l'écran, selon le système d'exploitation utilisé.
    """
    import os
    os.system('cls' if os.name == 'nt' else 'clear')


def quitterProgramme(codeSortie=0):
    print("\n\nAu revoir !")
    exit(codeSortie)


if __name__ == "__main__":
    menu()
